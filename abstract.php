<?php
// Parent class
abstract class hewan {
  public $name;
  public $kaki;
  public $darah = "50";
  public $keahlihan;
  public function __construct($name) {
    $this->name = $name;
  }
  abstract public function intro();
}

abstract class fight {
    public $attack;
    public $power;
    public $defence;
    public function __construct($attack) {
        $this->attack =$attack;
    }

    abstract public function intro();

} 

// Child classes
 class elang extends hewan {
  public function intro() : string {
    return "saya menyerang !  $this->name!";
  }
}

class harimau extends hewan {
  public function intro() : string {
    return "saya di serang $this->name!";
  }
}

// Create objects from the child classes
$harimau = new harimau("harimau");
echo $harimau->intro();
echo "<br>";

$elang = new elang ("elang");
echo $elang->intro();
echo "<br>";


?>