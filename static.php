<?php

class Lingkaran {

   public const PI = 3.14;

   public function luas($jari)
    {

     echo self::PI * $jari * $jari;

   }

}

echo Lingkaran::PI;

echo PHP_EOL;

$lingkaran = new Lingkaran();

$lingkaran->luas(7);

echo PHP_EOL;